import React, { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

const AppNavBar = () => {

   const {user} = useContext(UserContext)
    return (
     
            <Navbar bg="light" variant="light" sticky="top" className="navbar">
            <Container>
            <Navbar.Brand href="#home">Phillipine Animal Industry Information System </Navbar.Brand>
            <Nav className="mr-auto">
            {user.id !== null
            ?
            user.isAdmin === true ? <Link className="nav-link" to="/logout">Log Out</Link> :
            <React.Fragment>
            <Link className="nav-link" to="/products">Animal Products</Link>
            <Link className="nav-link" to="/inventory">Animal Inventory</Link>
            <Link className="nav-link" to="/users">User Accounts</Link>
            <Link className="nav-link" to="/logout">Log Out</Link>
            </React.Fragment>
            :
            <React.Fragment>
            <Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}}>Log In</Link>
            <Link className="nav-link" to="/register">Register</Link>
            </React.Fragment>
            }
            </Nav>
            </Container>
            </Navbar>


           );

}

export default AppNavBar;
