import React, { useState, useEffect } from "react"
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import { UserProvider } from './UserContext';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import UserAccounts from "./pages/UserAccounts";
import AnimallInventory from "./components/AnimallInventory";
import AnimalProducts from "./components/AnimalProducts";
import Footer from "./components/Footer";




function App() {

  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
  })


  useEffect (() => {
    fetch (`http://localhost:3001/api/users/details`, {
      headers: {Authorization: `${localStorage.getItem('token')}`}
    })
    .then (res => res.json())
    .then (data => {
      if (typeof data._id !== 'undefined'){
        setUser ({id: data._id, isAdmin: data.isAdmin})
      } else {
          setUser ({id: null, isAdmin: null})
      }

    })


  }, [])


  return (
    <UserProvider value={{user, setUser}}>
      <Router>
          <AppNavBar />
            ​​<Routes>
              <Route path="/" element={<Home />} />
              <Route path="/inventory" element={<AnimallInventory />} />
              <Route path="/products" element={<AnimalProducts />} />
              <Route path="/users" element={<UserAccounts />} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />}/>
              <Route element={Error}/>
        </Routes>
    </Router>   
    <Footer />
 </UserProvider>

  );
}

export default App;
