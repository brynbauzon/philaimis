import React from 'react';
import Banner from '../components/Banner'
//import Highlights from '../components/Highlights'
import { Container } from 'react-bootstrap'
 
 
const Home = () => {
   const pageData = {
       title: "PhilAIMIS",
       content: "Text Goes Here",
       destination: "",
       label: "Data Table"
   };
 
   return(
       <React.Fragment>
           <Banner data={pageData}/>
           <Container fluid>
               <h2 className="text-center mb-4">Featured Products</h2>
               {/* <Highlights /> */}
           </Container>
       </React.Fragment>
   );
}
export default Home;