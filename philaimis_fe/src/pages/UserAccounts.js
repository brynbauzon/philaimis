
import React, {useState, useEffect} from 'react'
import { Table, Container, Button, Modal, Form } from 'react-bootstrap'
import axios from 'axios'

const UserAccounts = () => {

  const fetchUrl='http://localhost:3001/api/users'
  const[users, setUsers]=useState([]);
  const [id, setId] = useState('');
  const[firstName, setFirstName]=useState([]);
  const[middleName, setMiddleName]=useState([]);
  const[lastName, setLastName]=useState([]);
  const[email, setEmail]=useState([]);
  const[password, setPassword]=useState([]);
  


  const [showAdd, setShowAdd] = useState(false);
  const handleCloseAdd = () => setShowAdd(false);
  const handleShowAdd = () => setShowAdd(true);


 


  useEffect(()=>{

    async function fetchData() {
            const data = await axios.get(fetchUrl)
            setUsers(data.data)
    }
    
    fetchData(fetchUrl)
    }, [])
    
  


    const addUser = (e) => {
      e.preventDefault();
      fetch(`http://localhost:3001/api/users/register`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token') }`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          firstName: firstName,
          middleName: middleName,
          lastName: lastName,
          email: email,
          password: password
        })
      })
      .then(res => res.json())
      .then(data => {
        if (data === true) {
          // this came from login.js. From login.js,  it redirectet to Products and from Products fetchData is passed
          // to AdminView as props				
          //fetchData();
     //   fetchData();
          alert("Product successfully added.");
          setFirstName('');
          setMiddleName('');
          setLastName('');
          setEmail('');
          setPassword('');
          handleCloseAdd();
        } else {
          alert("Something went wrong.");
          handleCloseAdd();
        }
      });
    };
    //console.log(users)


 

      const activateUser = (userId) => {
        fetch(`http://localhost:3001/api/users/${userId}/enable`, {
          method: 'PUT',
          headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`,
            'Content-Type': 'application/json'
          }
        })
        .then(res => res.json())
        .then(data => {
  
          if (data === true) {
           // fetchData();
            alert("User successfully activated.");
          } else {
            alert("Something went wrong.");
          }
        });
  
      };
  
      const disableUser = (userId) => {
      //  alert(userId)
  
        fetch(`http://localhost:3001/api/users/${userId}/disable`, {
          method: 'PUT',
          headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`,
          }
        })
        .then(res => res.json())
        .then(data => {
  
          if (data === true) {
        //    fetchData();
            alert("User successfully deactivated.");
          } else {
            alert("Something went wrong.");
          }
  
        });
  
      };
//  console.log(users.id)
  return (
    <Container>  
       <Table striped bordered hover>
          <thead>
            <tr>        
              <th>First Name</th>
              <th>Last Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
            <tbody>
              {users.map((users) => 
                  <tr key={users._id}>
                    <td>{users.firstName}</td>
                    <td>{users.middleName}</td>
                    <td>{users.lastName}</td>
                    <td>{users.email}</td>
                    <td><Button 
                        variant="primary" 
                        size="sm" >Update</Button>
                        { users.isActive ?
                              <Button 
                                variant="success"
                                size="sm"
                                onClick={() => activateUser(users._id)}
                              >
                                Enable
                              </Button>
                            :
                              <Button
                                variant="danger"
                                size="sm"
                                 onClick={() => disableUser(users._id)}
                              >
                                Disable
                              </Button>
                          }
                    </td>       
                  </tr>     
              )}
            </tbody>
        </Table>  


        <Button variant="primary" onClick={handleShowAdd}>
              Add User Acccounts
        </Button>
        

        {/*Modal for Add */}
      <Modal show={showAdd} onHide={handleCloseAdd}>
        <Form onSubmit={e => addUser(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add User Accounts</Modal.Title>
          </Modal.Header>
              <Modal.Body>
                  <Form.Label htmlFor="inputFirstName">First Name</Form.Label>
                  <Form.Control
                      type="text"
                      placeholder="Enter product name"
                      value={firstName}
                      onChange={e => setFirstName(e.target.value)}
                      required
                    />
                  <Form.Label htmlFor="inputMiddleName">Middle Name</Form.Label>
                  <Form.Control
                      type="text"
                      placeholder="Enter product name"
                      value={middleName}
                      onChange={e => setMiddleName(e.target.value)}
                      required
                    />
                  <Form.Label htmlFor="inputLastName">Last Name</Form.Label>
                  <Form.Control
                      type="text"
                      placeholder="Enter product name"
                      value={lastName}
                      onChange={e => setLastName(e.target.value)}
                      required
                    />
                  <Form.Label htmlFor="inputEmail">Email</Form.Label>
                  <Form.Control
                      type="email"
                      placeholder="Enter product name"
                      value={email}
                      onChange={e => setEmail(e.target.value)}
                      required
                    />
                    <Form.Label htmlFor="inputPassword">Password</Form.Label>
                  <Form.Control
                      type="password"
                      type="text"
                      placeholder="Enter product name"
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                      required
                    />  
              </Modal.Body>
              <Modal.Footer>
              <Button variant="secondary" onClick={handleCloseAdd}>
                Close
              </Button>
              <Button variant="success" type="submit">
                Save Changes
              </Button>
            </Modal.Footer>
            </Form>	
        </Modal>  

      
  </Container>
  )
}

export default UserAccounts
