//Use directive keyword require to make a module useful in the current file/module
const express = require('express');

//Router() handles the requests
const router = express.Router();


//Region
router.get('/regions', (req, res)   =>{

    var Request = require("request");

    Request.get("https://psgc.gitlab.io/api/regions/", (error, response, body) => {
        if(error) {
            return res.send(error);
        }
        res.send(JSON.parse(body));
    });

})


//Provinces
router.get('/provinces', (req, res)   =>{

    var Request = require("request");

    Request.get("https://psgc.gitlab.io/api/provinces/", (error, response, body) => {
        if(error) {
            return res.send(error);
        }
        res.send(JSON.parse(body));
    });

})



//Cities-Municipalities
router.get('/cities-municipalities', (req, res)   =>{

    var Request = require("request");

    Request.get("https://psgc.gitlab.io/api/cities-municipalities/", (error, response, body) => {
        if(error) {
            return res.send(error);
        }
        res.send(JSON.parse(body));
    });

})


//Barangays
router.get('/barangays', (req, res)   =>{

    var Request = require("request");

    Request.get("https://psgc.gitlab.io/api/barangays/", (error, response, body) => {
        if(error) {
            return res.send(error);
        }
        res.send(JSON.parse(body));
    });

})


//Epxports module of router
module.exports = router;