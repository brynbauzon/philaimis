//Use directive keyword require to make a module useful in the current file/module
const express = require('express');
const auth = require("../auth"); 
const bcrypt = require("bcryptjs");
let salt = bcrypt.genSaltSync(10);

//Router() handles the requests
const router = express.Router();

//User Controller goes here
const userAccountController = require('./../controllers/userAccountController');

//Register a user route
router.post('/register',(req, res)=>{

    //Expecting data/object coming from request body
    userAccountController.register(req.body).then(result => res.send(result))

})

//Login Routes
router.post('/login', (req, res) => {

        userAccountController.login(req.body).then(result => res.send(result))
})

router.get('/', (req, res) => {
    
    userAccountController.getAllUsers().then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	
	userAccountController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

router.put("/:userId/disable", auth.verify, (req, res) => { //need for middleware
	// if(auth.decode(req.headers.authorization).isAdmin === false) {
	// 	res.send(false);
	// } else {
		userAccountController.disableUser(req.params.userId).then(resultFromController => res.send(resultFromController))
	//}
})

router.put("/:userId/enable", auth.verify, (req, res) => { //need for middleware
	// if(auth.decode(req.headers.authorization).isAdmin === false) {
	// 	res.send(false);
	// } else {
		userAccountController.activateUser(req.params.userId).then(resultFromController => res.send(resultFromController))
//	}
})



module.exports = router;