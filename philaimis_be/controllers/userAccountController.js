const req = require('express/lib/request');
const UserAccount = require('./../models/userAccounts');
const auth = require('./../auth');


//For encrypting
const bcrypt = require('bcrypt')


module.exports.register = (registerData) =>{

        let userAccount = new UserAccount({

            firstName : registerData.firstName,
            middleName : registerData.middleName,
            lastName : registerData.lastName,
            birthDate: registerData.birthDate,
            email : registerData.email,
            password: bcrypt.hashSync(registerData.password, 10),
            isAdmin : registerData.isAdmin,
            isActive : registerData.isActive
        })

        return userAccount.save().then((result, error) => {
                console.log(result)

              if(result){

                return true
              }else{

                return false
              }


        })
}


module.exports.login = (loginData) => {

  const {email, password} = loginData

  return UserAccount.findOne({email: email}).then((result, error) => {
      //console.log(result)

    if(result == null){
          return false
    }else{
     
      let isPwCorrect = bcrypt.compareSync(password, result.password)	//boolean bec it compares two arguments

			if(isPwCorrect == true){
			//return json web token
				//invoke the function which creates the token upon logging in
				// requirements in creating a token:
					// if password matches from existing pw from db
					return {access: auth.createAccessToken(result)}
					// return auth.createAccessToken(result)
			} else {
				return false
			}

    }


  })

}


module.exports.getAllUsers = () => {
	return UserAccount.find().then( (result, error) => {
		if(result !== null){
			return result
		} else {
			return error
		}
	})
}

module.exports.getProfile = (data) => {
	return UserAccount.findById(data.userId).then(result => {
	//	result.password = undefined;
		return result;
	})
}

module.exports.disableUser = (userId) => {
	let updateActiveField = {
		isActive : false
	}

	return UserAccount.findByIdAndUpdate(userId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}


module.exports.activateUser = (userId) => {
	let updateActiveField = {
		isActive : true
	}

	return UserAccount.findByIdAndUpdate(userId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}