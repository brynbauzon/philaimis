//use of mongoose module
const mongoose = require("mongoose");

//var directive for invoking mongoose.Schema module
const userAccountsSchema = new mongoose.Schema({

    firstName : {

        type: String,
     //   required: [true, "Firstname is required"]
    },

    middleName : {

        type: String,
   //    required: [true, "Middlename is required"]

    },

    lastName : { 

        type: String,
 //      required: [true, "Lastname is required"]

    },

    birthDate : {

        type: Date,
  //      required: [true, "Birthday is required"],
        format: "MM/dd/yyyy"


    },
    
    email : {
        
        type: String,
        required: [true, "Email is required"],
        unique: true
    },

    password : {

        type:String,
        required: [true, "Password is required"]
    },

    isAdmin: {

        type: Boolean,
        default: false
    },

    isActive: {

        type: Boolean,
        default: false
    }

});

//module exports to use module 
module.exports = mongoose.model("UserAccounts", userAccountsSchema)