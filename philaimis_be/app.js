//Use directive keyword require to make a module useful in the current file/module
const express = require('express');

//Express function is our server stored in a constant variable app
const app = express();


//use mongoose module to be used in our connection to db
const mongoose = require('mongoose');

//allows us to relax the security applied to an API.
const cors = require('cors')

//PORT varialble to address location of port
const PORT = 3001

//Middlewares
//express.json is an express framework to parse incoming json payloads
app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(cors());

//Connecting userRoutes module to index.js entry point
const userAccountRoutes = require('./routes/userAccountRoutes');
const psgcAccountRoutes = require('./routes/psgcRoutes');


//connect to mongoDB database
//connect to mongoDB database
mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.fs2pp.mongodb.net/philaimis?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

//Connection string in mongodb cloud
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));

// //middleware entry point url (root url before any endpoints)
app.use("/api/users", userAccountRoutes);
// app.use("/api/products", productRoutes);
app.use("/api/psgc", psgcAccountRoutes);



//server listening to port 3001
app.listen(process.env.PORT || 3001, () => console.log (`Server is running at ${PORT}`));


