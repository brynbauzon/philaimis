import React from 'react'
import {Link} from 'react-router-dom'

const Header = () => {
  return (
    <div>
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    {/* <!-- Left navbar links --> */}
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block nav-link">
       <Link to='/'>Home</Link>
      </li>
      <li class="nav-item d-none d-sm-inline-block nav-link">
        <Link to='/users'>Users</Link>
      </li>
    </ul>

    {/* <!-- Right navbar links --> */}
    <ul class="navbar-nav ml-auto"> 
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Logout</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
  
    </ul>
  </nav>
  {/* <!-- /.navbar --> */}
    </div>
  )
}

export default Header
