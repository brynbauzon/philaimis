import {useState, useEffect} from 'react';
import { Form, Button, Row, Col, Card, DropdownButton, Dropdown, Select } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import axios from 'axios';
 
const Register = () => {
 
   const [firstName, setFirstName] = useState("");
   const [middleName, setMiddleName] = useState("");
   const [lastName, setlastName] = useState("");
   const [birthDate, setBirthDate] = useState("");
   
  
   const [region, setRegion] = useState([]);
   const [regionid, setRegionid]= useState('');
   const [province, setProvince]= useState([]);
   const [provId, setProvId]=useState('');
   const [brgyId, setBrgyId]=useState('');
   const [munCities, setMunCities]= useState([]);
   const [barangays, setBarangays]= useState([]);
   const [munCitiesId, setMunCitiesId]=useState('');


   const [email, setEmail] = useState("");
   const [password1, setPassword1] = useState("");
   const [password2, setPassword2] = useState("");
   const [error1, setError1] = useState(true);
   const [error2, setError2] = useState(true);
   const [isActive, setIsActive] = useState(false);
   const [willRedirect, setWillRedirect] = useState(false);
   
   



   const [inputValue, setInputValue] = useState("");
   const [selectedValue, setSelectedValue] = useState(null);
 
//    useEffect(() => {
 
// //        if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
// //            setIsActive(true);
// //        } else {
// //            setIsActive(false);
// //        }
 
// //    }, [email, password1, password2]);
 
// //    useEffect(() => {
 
// //        if (email === '' || password1 === '' || password2 === '') {
// //            setError1(true);
// //            setError2(false);
// //            setIsActive(false);
// //        } else if ((email !== '' && password1 !== '' && password2 !== '') && (password1 !== password2)) {
// //            setError1(false);
// //            setError2(true);
// //            setIsActive(false);
// //        } else if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
// //            setError1(false);
// //            setError2(false);
// //            setIsActive(true);
// //        }
 
// //    }, [email, password1, password2]);

// //    const registerUser = (e) => {
 
// //        e.preventDefault();
      
// //        fetch(`http://localhost:3001/api/users/register`, {
// //            method: 'POST',
// //            headers: { 'Content-Type': 'application/json' },
// //            body: JSON.stringify({

// //                firstName: firstName,
// //                middleName: middleName,
// //                lastName: lastName,
// //                birthDate: birthDate,
// //                email: email,
// //                password: password1
               
// //            })
// //        })
// //        .then(res => res.json())
// //        .then(data => {
 
// //            if (data === true) {
// //                alert("Registration successful. You may now log in.");
// //                setWillRedirect(true);
// //            } else {
// //                alert("Something went wrong.");
// //                setEmail("");
// //                setPassword1("");
// //                setPassword2("");
// //            }
// //        })
// //    }

//    const [value,setValue]=useState('');
//   const handleSelect=(e)=>{
//     console.log(e);
//     setValue(e)
//   }



  
  useEffect(()=>{
    
      const getRegions = async () => {

        const res = await fetch('https://psgc.gitlab.io/api/regions/')
        const getReg = await res.json();

        console.log(getReg)
        setRegion(await getReg);

      }


      getRegions();
  },[])


  const handlRegion=(event)=>{
    
    const getRegionId= event.target.value;

  
    console.log(getRegionId)
    
    setRegionid(getRegionId)
  
  


  }

  const handleProvince=(event)=>{
    
    const getProvId= event.target.value;
  

    console.log(getProvId) 
    setProvId(getProvId)


  }


  const handleMunCities = (event) => {

      const getMunCitiesId = event.target.value;
      console.log(getMunCitiesId)
      setMunCitiesId(getMunCitiesId)

  }


  const handleBrgys = (event) => {
    const getBrgyId = event.target.value;
    console.log(getBrgyId)
 
  }



 

  useEffect(()=>{

    const getprovince = async () => {
  
      const resProvince = await fetch(`https://psgc.gitlab.io/api/regions/${regionid}/provinces/`);
      const getProv = await resProvince.json();
      setProvince(await getProv);
   
    }
    getprovince();

  }, [regionid]);


  useEffect(()=>{

    const getMunCities  = async () => {

      const resMunCities = await fetch(`https://psgc.gitlab.io/api/provinces/${provId}/cities-municipalities/`)
      const getMunCities = await resMunCities.json();
      setMunCities(await getMunCities)
    }

   getMunCities();
   
  },[provId])


  useEffect(()=>{

    const getBarangays  = async () => {

      const resBarangays = await fetch(`https://psgc.gitlab.io/api/cities-municipalities/${munCitiesId}/barangays/`)
      const getBarangays = await resBarangays.json();
      setBarangays(await getBarangays)
    }

    getBarangays();
   
  }, [munCitiesId])



 


   return(
       <div><h1>This is Register</h1>
       <label>Region</label>
        <select name='regions' className='form-control' onChange={(e) => handlRegion(e)}>
           <option>--Select Region--</option>
           {
               region.map( (regionGet) => (

            <option key={regionGet.code} value={regionGet.code}>{regionGet.name} </option>

               ))}
        </select>
        <label>Province</label>
        <select name='province' className='form-control' onChange={(e) => handleProvince(e)}>
           <option>--Select Province--</option>
           {
               province.map( (provinceGet) => (

            <option key={provinceGet.code} value={provinceGet.code}>{provinceGet.name}</option>

               ))}
        </select>
        <label>Municipality</label>
        <select name='province' className='form-control' onChange={(e) => handleMunCities(e)}>
           <option>--Select Province--</option>
           {
               munCities.map( (munCitiesGet) => (

            <option key={munCitiesGet.code} value={munCitiesGet.code}>{munCitiesGet.name}</option>

               ))}
        </select>
        <label>Barangay</label>
        <select name='province' className='form-control'  onChange={(e) => handleBrgys(e)}>
           <option>--Select Province--</option>
           {
               barangays.map( (barangaysGet) => (

            <option key={barangaysGet.code} value={barangaysGet.code}>{barangaysGet.name}</option>

               ))}
        </select>
        
        
         </div> 
       
       );
 
      }
      export default Register;
/*   
   /* //    willRedirect === true ?
    //            <Navigate to={{pathname: '/login', state: { from: 'register'}}}/>
    //        :
    //            <Row className="justify-content-center">
    //                <Col xs md="6">
    //                    <h2 className="text-center my-4">Register</h2>
    //                    <Card>
    //                        <Form onSubmit={e => registerUser(e)}>
    //                            <Card.Body>
    //                            <Form.Group controlId="firstName">
    //                                    <Form.Label>First Name:</Form.Label>
    //                                    <Form.Control
    //                                        type="text"
    //                                        placeholder="Enter your first name" */
    //                                        value={firstName}
    //                                        onChange={e => setFirstName(e.target.value)}
    //                                        required
    //                                    />
    //                                </Form.Group>
    //                                <Form.Group controlId="middleName"> */}
    //                                    <Form.Label>Middle Name:</Form.Label>
    //                                    <Form.Control
    //                                        type="text"
    //                                        placeholder="Enter your middle name"
    //                                        value={middleName}
    //                                        onChange={e => setMiddleName(e.target.value)}
    //                                        required
    //                                    />
    //                                </Form.Group>
    //                                <Form.Group controlId="lastName">
    //                                    <Form.Label>Last Name:</Form.Label>
    //                                    <Form.Control
    //                                        type="text"
    //                                        placeholder="Enter your last name"
    //                                        value={lastName}
    //                                        onChange={e => setlastName(e.target.value)}
    //                                        required
    //                                    />
    //                                </Form.Group>
    //                                <Form.Group controlId="birthDate">
    //                                    <Form.Label>Birth Date:</Form.Label>
    //                                    <Form.Control
    //                                        type="date"
    //                                        placeholder="Enter your birthdate"
    //                                        value={birthDate}
    //                                        onChange={e => setBirthDate(e.target.value)}
    //                                        required
    //                                    />
    //                                </Form.Group>
    //                                <Form.Group controlId="userEmail">
    //                                    <Form.Label>Email:</Form.Label>
    //                                    <Form.Control
    //                                        type="email"
    //                                        placeholder="Enter your email"
    //                                        value={email}
    //                                        onChange={e => setEmail(e.target.value)}
    //                                        required
    //                                    />
    //                                </Form.Group>
    //                                <Form.Group controlId="password1">
    //                                    <Form.Label>Password:</Form.Label>
    //                                    <Form.Control
    //                                        type="password"
    //                                        placeholder="Enter your password"
    //                                        value={password1}
    //                                        onChange={e => setPassword1(e.target.value)}
    //                                        required
    //                                    />
    //                                </Form.Group>
    //                                <Form.Group controlId="password2">
    //                                    <Form.Label>Verify Password:</Form.Label>
    //                                    <Form.Control
    //                                        type="password"
    //                                        placeholder="Verify your password"
    //                                        value={password2}
    //                                        onChange={e => setPassword2(e.target.value)}
    //                                        required
    //                                    />
    //                                </Form.Group>
                            
 
    //                            </Card.Body>
    //                            <Card.Footer>
    //                                {isActive === true ?
    //                                        <Button
    //                                            variant="success"
    //                                            type="submit"
    //                                            block
    //                                        >
    //                                            Register
    //                                        </Button>
    //                                    :
    //                                        error1 === true || error2 === true ?
    //                                            <Button
    //                                                variant="danger"
    //                                                type="submit"
    //                                                disabled
    //                                                block
    //                                            >
    //                                                Please enter your registration details
    //                                            </Button>
    //                                        :
    //                                            <Button
    //                                                variant="danger"
    //                                                type="submit"
    //                                                disabled
    //                                                block
    //                                            >
    //                                                Passwords must match
    //                                            </Button>
    //                                }
    //                            </Card.Footer>
    //                        </Form>
    //                    </Card>
    //                    <p className="text-center mt-3">
    //                        Already have an account? <Link to={{pathname: '/login', state: { from: 'register'}}}>Click here</Link> to log in.
    //                    </p>
    //                </Col>
    //            </Row>

